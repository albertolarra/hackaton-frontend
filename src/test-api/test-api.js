import { LitElement, html } from "lit";

// html --> es un motor de plantilla

class TestApi extends LitElement {
  static get properties() {
    return {
      movies: { type: Array },
    };
  }
  constructor() {
    super();

    this.movies = [];

    // Llamamos a la API en el constructor
    this.getMovieData();
  }

  render() {
    return html` ${this.movies.map(
      (movie) =>
        html`<div>
          La pelicula ${movie.title} fue dirigida por ${movie.director}
        </div>`
    )}`;
  }

  getMovieData() {
    console.log("getMovieData");
    console.log("Obteniendo datos de las peliculas");

    // Vamos a trabajar con asincronia (promesas o con funciones async)

    let xhr = new XMLHttpRequest(); // Objeto AJAX

    //Pasamos por 4 estados
    // a una propiedad se le puede asignar una funcion
    // onload se ejecuta cuando el servidor contesta
    // Contesta el servidor ok
    xhr.onload = () => {
      if (xhr.status === 200) {
        console.log("Petición completada correctamente");

        let APIResponse = JSON.parse(xhr.responseText);
        // console.log(APIResponse);
        // viendo la api vemos que nos devuelve las pelicuales en results
        this.movies = APIResponse.results;
      }
    };

    xhr.open("GET", "https://swapi.dev/api/films");

    // Si queremos mandar un body/post va en el send
    // xhr.send();
    console.log("Fin del getMovieData");

    this.pelis = [
      {
        idFilm: "1",
        title: "pelicula1",
        photo: "",
        description: "descripcion1",
        yearFilm: 2000,
        rating: 1,
      },
      {
        idFilm: "2",
        title: "pelicula2",
        photo: "",
        description: "descripcion2",
        yearFilm: 2001,
        rating: 2,
      },
      {
        idFilm: "3",
        title: "pelicula3",
        photo: "",
        description: "descripcion3",
        yearFilm: 2002,
        rating: 3,
      },
    ];
  }
}

// Añadimos el customElements
customElements.define("test-api", TestApi);
