import { LitElement, html } from "lit";
// import "../peli-ficha-listado/peli-ficha-listado.js";

// html --> es un motor de plantilla

class ApiCall extends LitElement {
  static get properties() {
    return {
      // initial: { type: String },
      pelis: { type: Array },
      //mode: { type: Boolean },
    };
  }
  constructor() {
    super();

    this.pelis = [];
    //this.mode = false;
    // Llamamos a la API en el constructor
    this.getPeliData();
  }

  // render() {
  //   return html` <peli-ficha-listado ></peli-ficha-listado> `;
  // }

  updated(changedProperties) {
    console.log("updated en api-call");

    if (changedProperties.has("pelis")) {
      console.log("Ha cambiado el valor de la propiedad pelis en api call");
      // this.getPeliData();
      // this.pelis = this.mockPelisData();
      // console.log("pelis mock");
      // console.log(this.pelis);

      this.dispatchEvent(
        new CustomEvent("get-pelis", {
          detail: {
            pelis: this.pelis,
          },
        })
      );
    }
  }

  getPeliData() {
    console.log("getPeliData");
    console.log("Obteniendo datos de las peliculas");

    let xhr = new XMLHttpRequest();
    xhr.onload = () => {
      if (xhr.status === 200) {
        console.log("Petición completada correctamente en getPeliData");

        let APIResponse = JSON.parse(xhr.responseText);
        console.log(APIResponse);
        // viendo la api vemos que nos devuelve las pelicuales en results
        this.pelis = APIResponse;
        console.log("pelis api");
        console.log(this.pelis);
      }
    };
    // api
    // xhr.open("GET", "https://swapi.dev/api/films");

    //LocalHost
    xhr.open("GET", "http://localhost:8000/apitechufilms/v1/films");

    // Si queremos mandar un body/post va en el send
    xhr.send();

    // MOCK
    // this.pelis = this.mockPelisData();
    // console.log("pelis mock");
    // console.log(this.pelis);

    console.log("Fin del getPeliData");
  }

  createPeli(peli) {
    console.log("createPeli en api-call");
    console.log("La pelicula que vamos a crear tiene los siguientes campos: ");
    console.log(peli);
    let xhr = new XMLHttpRequest();
    xhr.onload = () => {
      if (xhr.status === 200) {
        console.log("Petición completada correctamente en createPeli");
      } else {
        console.log("Error en createPeli");
      }
    };

    //LocalHost
    xhr.open("POST", "http://localhost:8000/apitechufilms/v1/films");

    // Si queremos mandar un body/post va en el send
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.send(JSON.stringify(peli));
    console.log("Fin del createPeli");
  }

  deletePeli(id) {
    console.log("deletePeli en api-call");
    let xhr = new XMLHttpRequest();
    xhr.onload = () => {
      if (xhr.status === 200) {
        console.log("Petición completada correctamente en deletePeli");
      } else {
        console.log("Error en deletePeli");
      }
    };

    //LocalHost
    let peliDetele = "http://localhost:8000/apitechufilms/v1/films/" + id;
    console.log("peliDetele: " + peliDetele);
    xhr.open("DELETE", peliDetele);

    xhr.send();
    console.log("Fin del deletePeli");
  }

  mockPelisData() {
    console.log("funcion mockPelisData");
    return [
      {
        idFilm: "1",
        title: "Peli prueba1 MOD",
        photo:
          "https://es.web.img3.acsta.net/c_310_420/pictures/21/12/14/15/42/0496448.jpg",
        description: "t",
        yearFilm: 1991,
        rating: 4.5,
      },
      {
        idFilm: "2",
        title: "Peli prueba1 MOD",
        photo:
          "https://es.web.img3.acsta.net/c_310_420/pictures/21/12/14/15/42/0496448.jpg",
        description: "Descripcion peli2 mod",
        yearFilm: 1991,
        rating: 4.5,
      },
      {
        idFilm: "3",
        title: "Titulo peli3 MOD",
        photo:
          "https://es.web.img3.acsta.net/c_310_420/pictures/21/12/14/15/42/0496448.jpg",
        description: "Descripcion peli3",
        yearFilm: 1991,
        rating: 2.5,
      },
    ];
  }
}

// Añadimos el customElements
customElements.define("api-call", ApiCall);
