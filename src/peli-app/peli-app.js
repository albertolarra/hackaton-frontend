import { LitElement, html } from "lit";
import "../peli-main/peli-main.js";
import "../peli-header/peli-header.js";

class PeliApp extends LitElement {
  static get properties() {
    return {};
  }
  constructor() {
    super();
  }
  render() {
    return html`
      <link
        href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
        rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
        crossorigin="anonymous"
      />
      <peli-header></peli-header>
      <peli-main></peli-main>
    `;
  }
}
customElements.define("peli-app", PeliApp);
