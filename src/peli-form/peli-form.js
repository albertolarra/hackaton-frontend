import { LitElement, html } from "lit";

class PeliForm extends LitElement {
  static get properties() {
    return {
      peli: { type: Object },
    };
  }
  constructor() {
    super();

    this.resetFormData();
  }
  render() {
    return html`<link
    href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
    rel="stylesheet"
    integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
    crossorigin="anonymous"
    <br />
    <h2>Añade Tu pelicula</h2>
    <br />
    <div>
        <form>
            <div class="form-group">
                <label>Titulo</label>
                <input @input="${this.updateTitle}" type="text" class="form-control" placeholder="titulo"
                .value="${this.peli.title}"
                />
            </div>
            <div class="form-group">
                <label>Foto</label>
                <input @input="${this.updatePhoto}" type="text" class="form-control" placeholder="Introduce URL de la foto"
                />
            </div>
            <div class="form-group">
            <label>Descripcion</label>
            <input @input="${this.updateDescription}" type="text" class="form-control" placeholder="Descripcion"
            />
            <div class="form-group">
            <label>Año de Estreno</label>
            <input @input="${this.updateYearFilm}" type="text" class="form-control" placeholder="Año de Estreno"
            />
            <div class="form-group">
            <label>Puntuación</label>
            <input @input="${this.updateRating}" type="text" class="form-control" placeholder="Puntua de 1 a 5"
            />
            <button @click="${this.resetFormData}" class="btn btn-primary"><strong>Limpiar</strong></button>
            <button @click="${this.storeFilms}" class="btn btn-success"><strong>Guardar</strong></button>
        </div>
        </form>
    </div>
    `;
  }

  resetFormData() {
    console.log("resetFormData en peli-form");
    this.peli = {};
    this.peli.title = "";
    this.peli.photo = "";
    this.peli.description = "";
    this.peli.yearFilm = "";
    this.peli.rating = "";
  }

  updateTitle(e) {
    // console.log("updateTitle");
    // console.log(
    //   "Actualizando la propiedad titulo con el valor: " + e.target.value
    // );
    this.peli.title = e.target.value;
  }

  updatePhoto(e) {
    // console.log("updatePhoto");
    // console.log(
    //   "Actualizando la propiedad photo con el valor: " + e.target.value
    // );
    this.peli.photo = e.target.value;
  }

  updateDescription(e) {
    // console.log("updateDescription");
    // console.log(
    //   "Actualizando la propiedad descripction con el valor: " + e.target.value
    // );
    this.peli.description = e.target.value;
  }

  updateYearFilm(e) {
    // console.log("updateYearFilm");
    // console.log(
    //   "Actualizando la propiedad yearFilm con el valor: " + e.target.value
    // );
    this.peli.yearFilm = e.target.value;
  }

  updateRating(e) {
    // console.log("updateRating");
    // console.log(
    //   "Actualizando la propiedad rating con el valor: " + e.target.value
    // );
    this.peli.rating = e.target.value;
  }

  storeFilms(e) {
    console.log("storeFilms");
    e.preventDefault();

    console.log("La propiedad title vale " + this.peli.title);
    console.log("peli vale: ");
    console.log(this.peli);

    this.dispatchEvent(
      new CustomEvent("create-film", {
        detail: {
          peli: {
            idFilm: "",
            title: this.peli.title,
            photo: this.peli.photo,
            description: this.peli.description,
            yearFilm: this.peli.yearFilm,
            rating: this.peli.rating,
          },
        },
      })
    );
  }
}
customElements.define("peli-form", PeliForm);
