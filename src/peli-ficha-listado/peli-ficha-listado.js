import { LitElement, html } from "lit";

class PeliFichaListado extends LitElement {
  static get properties() {
    return {
      idFilm: { type: String },
      title: { type: String },
      photo: { type: String },
      description: { type: String },
      yearFilm: { type: Number },
      rating: { type: Number },
    };
  }
  constructor() {
    super();
  }
  render() {
    return html`
      <link
        href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
        rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
        crossorigin="anonymous"
      />

      <div class="card h-100">
        <input
          type="image"
          src="${this.photo}"
          width="50"
          alt="aaaa"
          class="card-img-top"
        />

        <div class="card-body">
          <h3 class="card-title">${this.title}</h3>
          <h5>Puntuación ${this.rating} de 5</h5>
          

          <p class="card-text">${this.description}</p>

          <ul class="list-group list-group-flush">
            <li class="list-group-item">${this.yearFilm}</li>
          </ul>
        </div>
        <div class="card-footer"></div>
        <button @click="${this.delete}" class="btn btn-danger col-5">
          <strong>-</strong>
        </button>
      </div>
    `;
  }
  delete() {
    console.log("delete en peli-ficha-listado");
    console.log("se va a borrar la feli de nombre " + this.idFilm);

    this.dispatchEvent(
      new CustomEvent("delete-peli", {
        detail: {
          idFilm: this.idFilm,
        },
      })
    );
  }
}
customElements.define("peli-ficha-listado", PeliFichaListado);
