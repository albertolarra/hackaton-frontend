import { LitElement, html } from "lit";
// import "../peli-ficha-listado/peli-ficha-listado.js";
import "../api-call/api-call.js";
import "../peli-ficha-listado/peli-ficha-listado.js";
import "../peli-form/peli-form.js";

class PeliMain extends LitElement {
  static get properties() {
    return {
      pelis: { type: Array },
    };
  }
  constructor() {
    super();

    this.pelis = [];
  }
  render() {
    return html`
      <link
        href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
        rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
        crossorigin="anonymous"
      />
      <main class="row row-cols-1 row-cols-sm-4">
        ${this.pelis.map(
          (i) => html` <peli-ficha-listado
            idFilm="${i.idFilm}"
            title="${i.title}"
            yearFilm="${i.yearFilm}"
            .photo="${i.photo}"
            description="${i.description}"
            rating="${i.rating}"
            @delete-peli="${this.deleteFilm}"
          ></peli-ficha-listado>`
        )}
      </main>
      <br />
      <api-call id="apiCall" @get-pelis="${this.getPelis}"></api-call>
      <peli-form @create-film="${this.createFilm}"></peli-form>
    `;
  }

  getPelis(e) {
    console.log("updatedPelis en peli-main");
    console.log(e.detail);
    this.pelis = e.detail.pelis;
    // this.shadowRoot.getElementById("apiCall").mode = true;
  }

  newPeli() {
    console.log("newPeli");
    this.dispatchEvent(new CustomEvent("new-film", {}));
  }

  createFilm(e) {
    console.log("createFilm");
    console.log(e.detail.peli);
    this.shadowRoot.querySelector("api-call").createPeli(e.detail.peli);
  }

  deleteFilm(e) {
    console.log("deleteFilm");
    console.log(e.detail.idFilm);
    e.preventDefault();
    this.shadowRoot.querySelector("api-call").deletePeli(e.detail.idFilm);
  }
}
customElements.define("peli-main", PeliMain);
